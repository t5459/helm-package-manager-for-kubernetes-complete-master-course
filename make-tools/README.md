# make-tools

## home, sweet home

    git@gitlab.com:m5658/make-tools.git

    echo include make-tools/Makefile > Makefile
    
## adopt me

    git subtree add --prefix make-tools git@gitlab.com:m5658/make-tools.git main --squash


## inherit updates from the mother ship

    git subtree pull --prefix make-tools git@gitlab.com:m5658/make-tools.git main --squash

## push our updates to the mother ship

    git subtree push --prefix make-tools git@gitlab.com:m5658/make-tools.git main --squash


